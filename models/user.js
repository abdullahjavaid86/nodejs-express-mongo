/** @format */

const { DataTypes } = require('sequelize');
const { db } = require('../src/db');

const userModel = db.sequelize.define('user', {
	firstName: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	lastName: {
		type: DataTypes.STRING,
		// allowNull defaults to true
	},
});

module.exports = { user: userModel };
