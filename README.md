# Setting up the app

## Initial Setup

- install node version 14.x
- install yarn by running `npm i -g yarn`
- install sequelize-cli by running `npm i -g sequelize-cli`
- run `cp ./config/.env.example .env` from the root of the project
- setup environment variable in .env specially `DATABASE_*`
- start the app by running `yarn start`

## Database Setup

- first of all setup database credentials in `./config/config.json` in development mode (preferably in all the modes)
- then run `sequelize-cli db:seed:all` to populate dummy data

### Notice

To check the data open the browser and enter <http://localhost:4000/api/v1/user/list>
