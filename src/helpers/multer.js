/** @format */

const multer = require('multer');
const { uniqueId } = require('lodash');

const storage = multer.diskStorage({
	destination(req, file, cb) {
		cb(null, './uploads');
	},
	filename(req, file, cb) {
		cb(null, `${Date.now()}_random_${uniqueId()}_${file.originalname}`);
	},
});

const fileFilter = (req, file, cb) => {
	// reject a file
	if (!file.originalname.match(/\.(png|jpg|jpeg|gif)$/)) {
		// upload only png and jpg format
		return cb(new Error('Only png, jpg, jpeg and gif are allowed'));
	}
	return cb(undefined, true);
	// const checkIfTypeValid = ['image/jpeg', 'image/png', 'image/jpg'].includes(file.mimetype);
	// cb(!checkIfTypeValid ? new Error('Only images are allowed') : null, checkIfTypeValid);
};

exports.upload = multer({
	storage,
	limits: {
		fileSize: 2000000,
	},
	fileFilter,
});

exports.contactUpload = multer({
	storage,
	limits: {
		fileSize: 2000000,
	},
});
