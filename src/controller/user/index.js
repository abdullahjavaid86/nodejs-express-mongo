/** @format */

const logger = require('../../helpers/logger');
const { user } = require('../../../models/user');

/* global
successResponse:readonly
errorResponse:readonly
*/

exports.getAll = async (req, res) => {
	try {
		const users = await user.findAll();

		res.status(200).send(successResponse(users));
	} catch (e) {
		res.status(e?.status ?? 500).send(
			errorResponse(e.message, e?.status ?? 500),
		);
		logger.error(e);
	}
};
