/** @format */

const router = require('express').Router();
const user = require('.');

router.get('/list', user.getAll);

// Export the router
module.exports = router;
