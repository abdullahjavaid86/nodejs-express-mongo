/** @format */

const router = require('express').Router();
const middleware = require('./middlewares');
const errors = require('./errors/error');
const healthRouter = require('./health/router');
const userRoutes = require('./controller/user/routes');

// Wire up middleware
router.use(middleware.doSomethingInteresting);

router.use('/health', healthRouter);
router.use('/api/v1/user', userRoutes);

// Wire up error-handling middleware
router.use(errors.errorHandler);
router.use(errors.nullRoute);

// Export the router
module.exports = router;
