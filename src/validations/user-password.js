/** @format */

const Joi = require('joi');

const Schema = Joi.object({
	old_password: Joi.string().min(8).max(20).required(),
	password: Joi.string()
		.min(8)
		.max(20)
		.disallow(Joi.ref('old_password'))
		.required(),
	password_confirmation: Joi.string()
		.min(8)
		.max(20)
		.allow(Joi.ref('password'))
		.required(),
});

exports.userPasswordUpdateValidation = Schema;
