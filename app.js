/** @format */

const express = require('express');

const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

const setupApp = require('./src');
const { connectDb } = require('./src/db');
const logger = require('./src/helpers/logger');

const port = process.env.PORT || 9000;

async function setup() {
	setupApp(app);

	// Database connection
	connectDb();

	app.use(express.static(`${__dirname}/uploads`));

	/* eslint-disable */
	io.on('connection', function (socket) {
		logger.info('someone connected');

		socket.on('disconnect', function () {
			logger.info('someone disconnected');
		});
	});
	/* eslint-enable */

	server.listen(port, () => {
		logger.info(`App is running on port: ${port}`);
	});
}

setup();
