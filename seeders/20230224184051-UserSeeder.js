/** @format */

/** @type {import('sequelize-cli').Migration} */
module.exports = {
	async up(queryInterface) {
		/**
		 * Add seed commands here.
		 *
		 * Example:
		 * await queryInterface.bulkInsert('People', [{
		 *   name: 'John Doe',
		 *   isBetaMember: false
		 * }], {});
		 */

		await queryInterface.bulkInsert('users', [
			{
				firstName: 'John',
				lastName: 'Wick',
				createdAt: new Date(),
				updatedAt: new Date(),
			},
		]);
	},

	// eslint-disable-next-line
	async down(queryInterface, Sequelize) {
		/**
		 * Add commands to revert seed here.
		 *
		 * Example:
		 * await queryInterface.bulkDelete('People', null, {});
		 */
	},
};
